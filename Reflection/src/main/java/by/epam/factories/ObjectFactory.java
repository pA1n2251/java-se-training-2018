package by.epam.factories;

import by.epam.ifaces.BaseObjectInterface;
import by.epam.annotations.Proxy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;

/**
 * The factory class is used to get an object instance for a
 * class provided as attribute.
 */
 public final class ObjectFactory {
    /**
     * A method to get an object for the
     * specified class. If the class has the @Proxy annotation applied then a
     * proxy object should be used instead of the common instance
     * @param objectClass specified class
     * @return object or proxy if the class has the @Proxy annotation applied
     */
    public static Object getInstanceOf(final Class objectClass) {
        Annotation annotation = objectClass.getAnnotation(Proxy.class);
        if (annotation != null) {
            Proxy proxyAnnotation = (Proxy) annotation;
            try {
                Class handlerClass = Class.forName(proxyAnnotation.invocationHandler());
                Constructor constructor = handlerClass.getConstructor();
                InvocationHandler handler = (InvocationHandler) constructor.newInstance();
                BaseObjectInterface proxy = (BaseObjectInterface) java.lang.reflect.Proxy.newProxyInstance(
                        BaseObjectInterface.class.getClassLoader(),
                        new Class[] {BaseObjectInterface.class},
                        handler);
                return proxy;
            } catch (ClassNotFoundException
                    | NoSuchMethodException
                    | InvocationTargetException
                    | IllegalAccessException
                    | InstantiationException e) {
                e.printStackTrace();
                throw new IllegalArgumentException(e.toString(), e);
            }
        } else {
            try {
                Constructor constructor = objectClass.getConstructor();
                return constructor.newInstance();
            } catch (NoSuchMethodException
                    | InvocationTargetException
                    | IllegalAccessException
                    | InstantiationException e) {
                e.printStackTrace();
                throw new IllegalArgumentException(e.toString(), e);
            }
        }
    }

    private ObjectFactory() {

    }
}
