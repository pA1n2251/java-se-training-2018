package by.epam;

import by.epam.annotations.Equal;

import java.lang.reflect.Field;

/**
 * The class expects to get objects and makes
 * fields scanning. Returns true if the objects are equal.
 */
public final class Analyzer {
    /**
     * Method uses a @Equal annotation to
     * make a decision of including a field in the result
     * @param firstObject first equal object
     * @param secondObject second equal object
     * @return Returns true if the objects are equal
     */
    public static boolean equalObjects(final Object firstObject, final Object secondObject) {
        try {
            Boolean iteratorFlag = false;
            Class firstObjectClass = firstObject.getClass();
            Class secondObjectClass = secondObject.getClass();
            Field[] firstObjectFields = firstObjectClass.getDeclaredFields();
            for (Field firstObjectField : firstObjectFields) {
                firstObjectField.setAccessible(true);
                Equal equalAnnotation = firstObjectField.getAnnotation(Equal.class);
                if (equalAnnotation != null) {
                    Field secondObjectField = secondObjectClass.getDeclaredField(firstObjectField.getName());
                    if (secondObjectField != null) {
                        firstObjectField.setAccessible(true);
                        secondObjectField.setAccessible(true);
                        Object firstObjectFieldValue = firstObjectField.get(firstObject);
                        Object secondObjectFieldValue = secondObjectField.get(secondObject);
                        if (equalAnnotation.compareby()) {
                            if (firstObjectFieldValue != (secondObjectFieldValue)) {
                                return false;
                            } else {
                                iteratorFlag = true;
                            }
                        } else {
                            if (!firstObjectFieldValue.equals(secondObjectFieldValue)) {
                                return false;
                            } else {
                                iteratorFlag = true;
                            }
                        }
                    } else {
                        return false;
                    }
                }
            }
            return iteratorFlag;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }
    }

    private Analyzer() {
    }
}
