package by.epam;

import by.epam.beans.AnnotatedClass;
import by.epam.beans.FirstEqualObject;
import by.epam.beans.SecondEqualObject;
import by.epam.factories.ObjectFactory;
import by.epam.ifaces.BaseObjectInterface;

/**
 * Main class in the app
 */
public final class Main {
    /**
     * Main method
     * @param args arguments
     */
    public static void main(final String[] args) {
        System.out.println("Part A");
        BaseObjectInterface returnedObject = (BaseObjectInterface) ObjectFactory.getInstanceOf(AnnotatedClass.class);
        returnedObject.setName("Oleg");
        System.out.println(returnedObject);
        System.out.println("Part B");
        FirstEqualObject firstEqualObject = new FirstEqualObject();
        SecondEqualObject secondEqualObject = new SecondEqualObject();
        firstEqualObject.setName("Oleg");
        secondEqualObject.setName("Oleg");
        System.out.println(Analyzer.equalObjects(firstEqualObject, secondEqualObject));
    }
    private Main() {
    }
}
