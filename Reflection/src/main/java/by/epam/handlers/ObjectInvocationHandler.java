package by.epam.handlers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.logging.Logger;

/**
 * Handler used to create a proxy
 */
public class ObjectInvocationHandler implements InvocationHandler {

    private static final Logger LOG = Logger.getLogger(by.epam.handlers.ObjectInvocationHandler.class.getName());

    /**
     * Processes a method invocation on a proxy instance and returns the result.
     * @param proxy the proxy instance that the method was invoked on.
     * @param method the Method instance corresponding to the interface method invoked on the proxy instance.
     * @param args an array of objects containing the values of the arguments passed in the method invocation on the
     *             proxy instance, or null if interface method takes no arguments.
     * @return the value to return from the method invocation on the proxy instance.
     * @throws Throwable the exception to throw from the method invocation on the proxy instance.
     */
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        LOG.info("This is proxy");
        return null;
    }
}
