package by.epam.beans;

import by.epam.ifaces.BaseObjectInterface;

/**
 * Class without Proxy annotation
 */
public class NonAnnotatedClass implements BaseObjectInterface {
    private String name;

    /**
     * Constructor without params
     */
    public NonAnnotatedClass() {
    }

    /**
     * Constructor with params
     * @param name some name
     */
    public NonAnnotatedClass(final String name) {
        this.name = name;
    }

    /**
     * classical getters
     * @return some name
     */
    public String getName() {
        return name;
    }

    /**
     * Classical setters
     * @param name some name
     */
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NonAnnotatedClass{"
                + "name='"
                + name + '\''
                + '}';
    }
}
