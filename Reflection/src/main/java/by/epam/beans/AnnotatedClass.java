package by.epam.beans;

import by.epam.ifaces.BaseObjectInterface;
import by.epam.annotations.Proxy;

/**
 * Class with Proxy annotation
 */
@Proxy(invocationHandler = "by.epam.handlers.ObjectInvocationHandler")
public class AnnotatedClass implements BaseObjectInterface {
    private String name;

    /**
     * Constructor without params
     */
    public AnnotatedClass() {
    }

    /**
     * Constructor with params
     * @param name some name
     */
    public AnnotatedClass(final String name) {
        this.name = name;
    }

    /**
     * classical getters
     * @return some name
     */
    public String getName() {
        return name;
    }

    /**
     * Classical setters
     * @param name some name
     */
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AnnotatedClass{"
                + "name='"
                + name + '\''
                + '}';
    }
}
