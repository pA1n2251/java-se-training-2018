package by.epam.beans;
/**
 * Second class to test equal annotation
 */
public class SecondEqualObject {
    private int id;
    private String name;
    private String city;
    private long money;

    /**
     * Constructor without param
     */
    public SecondEqualObject() {
    }

    /**
     * Constructor with param
     * @param id int id
     * @param name String name
     * @param city String city
     * @param money long money
     */
    public SecondEqualObject(final int id, final String name, final String city, final long money) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.money = money;
    }
    /**
     * Classical getter
     * @return id
     */
    public int getId() {
        return id;
    }
    /**
     * Classical setter
     * @param id int id
     */
    public void setId(final int id) {
        this.id = id;
    }
    /**
     * Classical getter
     * @return name
     */
    public String getName() {
        return name;
    }
    /**
     * Classical setter
     * @param name String name
     */
    public void setName(final String name) {
        this.name = name;
    }
    /**
     * Classical getter
     * @return city
     */
    public String getCity() {
        return city;
    }
    /**
     * Classical setter
     * @param city String city
     */
    public void setCity(final String city) {
        this.city = city;
    }
    /**
     * Classical getter
     * @return money
     */
    public long getMoney() {
        return money;
    }
    /**
     * Classical setter
     * @param money long money
     */
    public void setMoney(final long money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "SecondEqualObject{"
                + "id=" + id + ", name='" + name + '\''
                + ", city='" + city + '\''
                + ", money=" + money + '}';
    }
}
