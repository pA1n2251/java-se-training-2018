package by.epam.beans;

import by.epam.annotations.Equal;

/**
 * First class to test equal annotation
 */
public class FirstEqualObject {
    private int id;
    @Equal(compareby = false) private String name;
    private int age;

    /**
     * Constructor without param
     */
    public FirstEqualObject() {
    }

    /**
     * Constructor with param
     * @param id int id
     * @param name String name
     * @param age int age
     */
    public FirstEqualObject(final int id, final String name, final int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    /**
     * Classical getter
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Classical setter
     * @param id int id
     */
    public void setId(final int id) {
        this.id = id;
    }
    /**
     * Classical getter
     * @return name
     */
    public String getName() {
        return name;
    }
    /**
     * Classical setter
     * @param name String name
     */
    public void setName(final String name) {
        this.name = name;
    }
    /**
     * Classical getter
     * @return age
     */
    public int getAge() {
        return age;
    }
    /**
     * Classical setter
     * @param age int age
     */
    public void setAge(final int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "FirstEqualObject{"
                + "id=" + id
                + ", name='" + name + '\''
                + ", age=" + age + '}';
    }

}
