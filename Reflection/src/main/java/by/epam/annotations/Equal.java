package by.epam.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation is used to mark fields to
 * be included in the process of objects comparing.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Equal {
    /**
     * To determine objects equality by reference or by value
     * @return boolean value. True - to determine objects equality by reference and
     * false - to determine objects equality by value (state)
     */
    boolean compareby();
}
