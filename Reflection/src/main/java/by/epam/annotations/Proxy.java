package by.epam.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation may appear on classes
 * and declares that a holder class can become proxy.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Proxy {
    /**
     * a class name for using as handler
     * @return String name for using as handler
     */
    String invocationHandler();
}
