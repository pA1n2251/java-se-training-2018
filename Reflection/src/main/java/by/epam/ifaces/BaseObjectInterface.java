package by.epam.ifaces;

/**
 * Interface to create a proxy
 */
public interface BaseObjectInterface {
    /**
     * Classical setters
     * @param name some name
     */
    void setName(String name);
    /**
     * classical getters
     * @return some name
     */
    String getName();
}
