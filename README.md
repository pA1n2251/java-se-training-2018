# Java Training 2018 Oleg Kameisha

## Reflection task
Java version - 1.8
Maven version - 7
To start just start main class

## Ant task
Java version - 1.8
Ant version - 1.10.3
To start just type ant

### Targets:
ant - to build and validate;
build-project - to build project;
validate-buildfile - to validate build file.
