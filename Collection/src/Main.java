/**
 * Main app class
 */
public final class Main {
    /**
     * Main app method
     * @param args in app
     */
    public static void main(final String[] args) {
        System.out.println("Hello World!");
        LRUConcurrentCache<Integer, String> lruCache = new LRUConcurrentCache<>(2);
        LFUConcurrentCache<Integer, String> lfuCache = new LFUConcurrentCache<>(4, 0.4);
        Runnable task1 = () -> {
            lruCache.put(1, "Oleg");
            lruCache.put(2, "Kameisha");
            lfuCache.set(1, "Oleg");
            lfuCache.set(2, "Kameisha");
            System.out.println(lruCache);
            System.out.println(lfuCache);
        };
        Runnable task2 = () -> {
            System.out.println("LFU get " + lfuCache.get(1));
            System.out.println("LFU get " + lfuCache.get(1));
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("LFU get " + lfuCache.get(2));
            System.out.println("LFU get " + lfuCache.get(2));
            System.out.println("LFU get " + lfuCache.get(1));
            System.out.println("LFU get " + lfuCache.get(1));
            System.out.println("LFU get " + lfuCache.get(1));
            lfuCache.set(3, "Ivanovich");
            lruCache.put(3, "Ivanovich");
            System.out.println(lruCache);
            System.out.println(lfuCache);
        };
        Thread firstThread = new Thread(task1);
        Thread secondThread = new Thread(task2);
        firstThread.start();
        secondThread.start();
    }
    private Main() {
    }
}
