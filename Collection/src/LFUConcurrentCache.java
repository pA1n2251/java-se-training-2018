import javafx.util.Pair;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * A simple thread-safe LFU cache
 * @param <K> key
 * @param <V> value
 */
public class LFUConcurrentCache<K, V> {
    // a hash map holding <key, <frequency, value>> pairs
    private final Map<K, Pair<Integer, V>> cache;

    // a list of LinkedHashSet, freqList[i] has elements with frequency = i
    private final LinkedHashSet<K>[] freqList;

    // the minimum frequency in the cache
    private int minFreq;

    // the size of the cache; it is also the upper bound of possible frequency
    private final int capacity;

    // the number of evicted elements when reaching capacity
    private final int evictNum;

    /**
     * Create a new LFU cache.
     * @param  cap           the size of the cache
     * @param  evictFactor   the percentage of elements for replacement
     */
    public LFUConcurrentCache(final int cap, final double evictFactor) {
        if (cap <= 0 || evictFactor <= 0 || evictFactor >= 1) {
            throw new IllegalArgumentException("Eviction factor or Capacity is illegal.");
        }
        capacity = cap;
        minFreq = 0;  // the initial smallest frequency
        evictNum = Math.min(cap, (int) Math.ceil(cap * evictFactor));

        cache = new HashMap<K, Pair<Integer, V>>();
        freqList = new LinkedHashSet[cap];
        for (int i = 0; i < cap; ++i) {
            freqList[i] = new LinkedHashSet<K>();
        }
    }

    /**
     * Update frequency of the key-value pair in the cache if the key exists.
     * Increase the frequency of this pair and move it to the next frequency set.
     * If the frequency reaches the capacity, move it the end of current frequency set.
     */
    private synchronized void touch(final K key) {
        if (cache.containsKey(key)) { // sanity checking
            int freq = cache.get(key).getKey();
            V val = cache.get(key).getValue();
            // first remove it from current frequency set
            freqList[freq].remove(key);

            if (freq + 1 < capacity) {
                // update frequency
                cache.put(key, new Pair<>(freq + 1, val));
                freqList[freq + 1].add(key);
                if (freq == minFreq && freqList[minFreq].isEmpty()) {
                    // update current minimum frequency
                    ++minFreq;
                }
            } else {
                // LRU: put the most recent visited to the end of set
                freqList[freq].add(key);
            }
        }
    }

    /**
     * Evict the least frequent elements in the cache
     * The number of evicted elements is configured by eviction factor
     * @return
     */
    private synchronized void evict() {
        for (int i = 0; i < evictNum && minFreq < capacity; ++i) {
            // get the first element in the current minimum frequency set
            K key = (K) freqList[minFreq].iterator().next();
            freqList[minFreq].remove(key);
            cache.remove(key);
            while (minFreq < capacity && freqList[minFreq].isEmpty()) {
                // skip empty frequency sets
                ++minFreq;
            }
        }
    }

    /**
     * Get the value of key.
     * If the key does not exist, return null.
     * @param  key   the key to query
     * @return the value of the key
     */
    public synchronized V get(final K key) {
        if (!cache.containsKey(key)) {
            return null;
        }
        // update frequency
        touch(key);
        return cache.get(key).getValue();
    }

    /**
     * Set key to hold the value.
     * If key already holds a value, it is overwritten.
     * @param  key   the key of the pair
     * @param  value the value of the pair
     */
    public synchronized void set(final K key, final V value) {
        if (cache.containsKey(key)) {
            Integer freq = cache.get(key).getKey();
            cache.put(key, new Pair<>(freq, value));  // update value
            touch(key);  // update frequency
            return;
        }
        if (cache.size() >= capacity) {
            evict();
        }
        // set the minimum frequency back to 0
        minFreq = 0;
        cache.put(key, new Pair<>(minFreq, value));
        freqList[minFreq].add(key);
    }

    @Override
    public String toString() {
        return "LFUConcurrentCache{" +
                "cache=" + cache +
                '}';
    }
}
