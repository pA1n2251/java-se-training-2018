import java.util.LinkedHashMap;
import java.util.Map;

/**
 * LRU Concurrent Cache
 * @param <K> key
 * @param <V> value
 */
public class LRUConcurrentCache<K, V> {
    private final Map<K, V> cache;

    /**
     * Constructs an empty insertion-ordered LRUConcurrentCache instance with the specified initial capacity and a
     * default load factor (0.75)
     * @param maxEntries the max entries in cache
     */
    public LRUConcurrentCache(final int maxEntries) {
        this.cache = new LinkedHashMap<K, V>(maxEntries, 0.75F, true) {
            @Override
            protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
                return size() > maxEntries;
            }
        };
    }

    /**
     * Associates the specified value with the specified key in this map. If the map previously contained a
     * mapping for the key, the old value is replaced.
     * @param key with which the specified value is to be associated
     * @param value to be associated with the specified key
     */
    public void put(final K key, final V value) {
        synchronized (cache) {
            cache.put(key, value);
        }
    }

    /**
     * Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
     * @param key the key whose associated value is to be returned
     * @return the value to which the specified key is mapped, or null if this map contains no mapping for the key.
     */
    public V get(final K key) {
        synchronized (cache) {
            return cache.get(key);
        }
    }

    @Override
    public String toString() {
        return "LRUConcurrentCache{" +
                "cache=" + cache +
                '}';
    }

}
