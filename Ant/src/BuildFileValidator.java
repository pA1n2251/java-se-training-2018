import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * This class allow custom task <buildfilevalidator>
 */
public class BuildFileValidator extends Task {
    private final Logger log =
            Logger.getLogger(BuildFileValidator.class.getName());

    private boolean checkDefault;
    private boolean checkName;
    private List<BuildFile> buildFiles = new ArrayList<>();

    /**
     * This method classical setter
     * @param checkDefault - this param let check attribute default
     */
    public void setCheckDefault(final boolean checkDefault) {
        this.checkDefault = checkDefault;
    }

    /**
     * This method classical setter
     * @param checkName - this param let check attribute name
     */
    public void setCheckName(final boolean checkName) {
        this.checkName = checkName;
    }

    @Override
    public void execute() throws BuildException {
        Project project;
        log.info("Start validate");
        for (BuildFile buildFile: buildFiles) {
            project = new Project();
            File location = new File(buildFile.getLocation());
            ProjectHelper.configureProject(project, location);
            if (checkDefault) {
                checkDefault(project);
            }
            if (checkName) {
                checkName(project);
            }
        }
        log.info("Validate successful");
    }

    /**
     * Method to check default target
     * @param project - project with path to build file
     */
    public void checkDefault(final Project project) {
        log.info("Def targ - " + project.getDefaultTarget());
        if (project.getDefaultTarget() == null) {
            throw new BuildException("No project default attribute set");
        }
    }

    /**
     * Method to check project name
     * @param project - project with path to build file
     */
    public void checkName(final Project project) {
        log.info("Def name - " + project.getName());
        if (project.getName() == null) {
            throw new BuildException("No project name attribute set");
        }
    }

    /**
     * A method that create build file instance
     * @return build file instance
     */
    public BuildFile createBuildFile() {
        BuildFile buildFile = new BuildFile();
        buildFiles.add(buildFile);
        return buildFile;
    }

    /**
     * Inner class to have inner element in task
     */
    public class BuildFile {
        /**
         * This is non-argument constructor
         */
        public BuildFile() {
        }

        private String location;

        /**
         * This is classical setter
         * @param location - path to validate file
         */
        public void setLocation(final String location) {
            this.location = location;
        }

        /**
         * Classical getter
         * @return string with path to validate file
         */
        public String getLocation() {
            return location;
        }
    }

}
