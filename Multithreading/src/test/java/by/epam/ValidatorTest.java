package by.epam;

import by.epam.beans.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.*;

public class ValidatorTest extends Mockito{

    private static final Validator validator = new Validator();

    @BeforeClass
    public static void setUp() {
        Building building = new Building();
        building.setPassengersNumber(2);
        validator.setBuilding(building);
    }

    @Test
    public void testValidateAllDispatchContainerEmpty() {
        CopyOnWriteArrayList<Story> stories = new CopyOnWriteArrayList<>();
        Story firstStory = new Story(0);
        Story secondStory = new Story(1);
        stories.add(firstStory);
        stories.add(secondStory);
        assertTrue(validator.validateAllDispatchContainerEmpty(stories));
    }

    @Test
    public void testValidateElevatorContainerEmpty() {
        Elevator elevator = new Elevator(1, 1);
        assertTrue(validator.validateElevatorContainerEmpty(elevator));
    }

    @Test
    public void testValidateDestinationContainerEqualArrivalStoryAllPassengers() {
        CopyOnWriteArrayList<Story> stories = new CopyOnWriteArrayList<>();
        Story firstStory = new Story(0);
        Story secondStory = new Story(1);
        Passenger firstPassenger = mock(Passenger.class);
        Passenger secondPassenger = mock(Passenger.class);
        when(firstPassenger.getDestinationStory()).thenReturn(0);
        when(secondPassenger.getDestinationStory()).thenReturn(1);
        firstStory.getArrivalStoryContainer().add(firstPassenger);
        secondStory.getArrivalStoryContainer().add(secondPassenger);
        stories.add(firstStory);
        stories.add(secondStory);
        assertTrue(validator.validateDestinationContainerEqualArrivalStoryAllPassengers(stories));
    }

    @Test
    public void testValidateAllPassengerInDestinationContainerComplete() {
        CopyOnWriteArrayList<Story> stories = new CopyOnWriteArrayList<>();
        Story story = new Story(0);
        Passenger passenger = mock(Passenger.class);
        when(passenger.getTransportationState()).thenReturn(TransportationState.COMPLETED);
        story.getArrivalStoryContainer().add(passenger);
        stories.add(story);
        stories.add(story);
        assertTrue(validator.validateAllPassengerInDestinationContainerComplete(stories));
    }

    @Test
    public void testValidatePassengersNumberInDispatchStoriesEqualPassengerNumber() {
        CopyOnWriteArrayList<Story> stories = new CopyOnWriteArrayList<>();
        Story story = new Story(0);
        Passenger passenger = mock(Passenger.class);
        story.getArrivalStoryContainer().add(passenger);
        stories.add(story);
        stories.add(story);
        assertTrue(validator.validatePassengersNumberInDispatchStoriesEqualPassengerNumber(stories));
    }

}
