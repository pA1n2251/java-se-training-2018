package by.epam.bean;

import by.epam.beans.Elevator;
import by.epam.beans.Passenger;
import by.epam.beans.TransportationState;
import org.junit.Test;
import static org.junit.Assert.*;


public class ElevatorTest {
    private final Elevator elevator = new Elevator(8, 8);

    @Test
    public void testMoveElevatorUp(){
        elevator.up();
        assertEquals(1, elevator.getStory());
    }

    @Test
    public void testMoveElevatorDown(){
        elevator.up();
        elevator.down();
        assertEquals(0, elevator.getStory());
    }

    @Test
    public void testAddPassengerElevatorTaskInProgress() {
        Passenger passengerAdd = new Passenger(1, 1, 2);
        elevator.addPassenger(passengerAdd);
        Passenger passengerGet = elevator.getPassenger(passengerAdd);
        assertEquals(TransportationState.IN_PROGRESS, passengerGet.getTransportationState());
    }

}
