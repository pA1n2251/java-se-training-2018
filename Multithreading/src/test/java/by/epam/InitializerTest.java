package by.epam;

import by.epam.beans.Story;
import by.epam.beans.TransportationState;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.Assert.*;

public class InitializerTest {
    private Initializer initializer;

    @Before
    public void setUp() {
        initializer = new Initializer("src/main/resources/test.properties");
    }

    @Test
    public void testGetStoriesNumber() {
        assertEquals(4, initializer.getStoriesNumber());
    }

    @Test
    public void testPassengersNumberInDispatchStoryEqualPassengerNumber() {
        long size = initializer.getStories().stream()
                .mapToInt(s -> s.getDispatchStoryContainer().size())
                .sum();
        assertEquals(20, size);
    }

    @Test
    public void testDispatchContainerNotEqualDestinationContainerAllPassengers() {
        boolean equal = initializer.getStories().stream()
                .map(Story::getArrivalStoryContainer)
                .flatMap(ConcurrentLinkedQueue::stream)
                .allMatch(passenger -> passenger.getDestinationStory() != passenger.getDispatchStory());
        assertTrue(equal);
    }
}
