import by.epam.beans.Building;
import by.epam.Validator;

/**
 * Main class in app
 */
public final class Main {
    private Main() {
    }
    /**
     * Main method in app
     * @param args - string args
     */
    public static void main(final String[] args) {
        Building building = new Building();
        building.startTransportation();
        new Validator(building).validate();
    }
}
