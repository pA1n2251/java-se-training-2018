package by.epam;

import by.epam.beans.Elevator;
import by.epam.beans.Passenger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Controller, which control transportation
 */
public class Controller {
    private final Logger logger = LoggerFactory.getLogger(Controller.class);
    private final CountDownLatch countDownLatch;
    private final ReentrantLock lock = new ReentrantLock(true);
    private final Condition conditionMove = lock.newCondition();
    private final Condition conditionDeboardLast = lock.newCondition();
    private final Condition conditionDeboard = lock.newCondition();

    private boolean flagUp = true;
    private Elevator elevator;

    /**
     * Creates a new Controller with the given initial value.
     * @param storiesNumber the initial value
     * @param elevatorCapacity the initial value
     */
    public Controller(final int storiesNumber, final int elevatorCapacity) {
        this.elevator = new Elevator(storiesNumber, elevatorCapacity);
        this.countDownLatch  = new CountDownLatch(elevatorCapacity + 1);
    }

    /**
     * Add passenger to elevator container. If elevator full than permit move elevator
     * @param passenger which will added to elevator container
     */
    public void addPassengerToElevator(final Passenger passenger) {
            logger.info("Boarding passanger " + passenger.getPassengerId()
                    + " (dispatch story - " + (passenger.getDispatchStory() + 1)
                    + ", destination story - " + (passenger.getDestinationStory() + 1) + ")"
                    + " on story " + (elevator.getStory() + 1));
            elevator.addPassenger(passenger);
            if (elevator.getElevatorContainer().size() == elevator.getElevatorCapacity()) {
                permitMove();
            }
    }

    /**
     * Permit move elevator
     */
    public void permitMove() {
        lock.lock();
        try {
            conditionMove.signal();
            conditionDeboardLast.await();
        } catch (InterruptedException e) {
            throw  new RuntimeException(e.getMessage(), e);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Gets and remove passenger from elevator container like example passenger
     * @param passengerExample example passenger
     * @return passenger from elevator container like example passenger
     */
    public Passenger getAndRemovePassengerFromElevator(final Passenger passengerExample) {
        logger.info("Deboarding passenger " + passengerExample.getPassengerId()
                + " (dispatch story - " + (passengerExample.getDispatchStory() + 1)
                + ", destination story - " + (passengerExample.getDestinationStory() + 1) + ")"
                + " on story " + (elevator.getStory() + 1));
        return elevator.getAndRemovePassenger(passengerExample);
    }

    /**
     * Move elevator up or down
     */
    public void moveElevator() {
        try {
            logger.info("Elevator on story " + (elevator.getStory() + 1));
            countDownLatch.countDown();
            countDownLatch.await();
            lock.lock();
            try {
                //Signals to everyone about the exit from the elevator
                conditionDeboard.signalAll();
                conditionDeboardLast.signalAll();
                //Waiting for the end of boarding and deboarding
                conditionMove.await(10, TimeUnit.MILLISECONDS);
            } finally {
                lock.unlock();
            }
            //Move elevator
            if (flagUp) {
                elevator.up();
                if (elevator.getStory() == elevator.getStoriesNumber() - 1) {
                    flagUp = false;
                }
            } else {
                elevator.down();
                if (elevator.getStory() == 0) {
                    flagUp = true;
                }
            }
        } catch (InterruptedException e) {
            throw  new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Gets the current value.
     * @return the current value
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * Gets the current value.
     * @return the current value
     */
    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }

    /**
     * Gets the current value.
     * @return the current value
     */
    public ReentrantLock getLock() {
        return lock;
    }

    /**
     * Gets the current value.
     * @return the current value
     */
    public Condition getConditionMove() {
        return conditionMove;
    }

    /**
     * Gets the current value.
     * @return the current value
     */
    public Condition getConditionDeboardLast() {
        return conditionDeboardLast;
    }

    /**
     * Gets the current value.
     * @return the current value
     */
    public Condition getConditionDeboard() {
        return conditionDeboard;
    }


    @Override
    public String toString() {
        return "Controller{" + "elevator=" + elevator + '}';
    }
}
