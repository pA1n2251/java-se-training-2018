package by.epam.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Elevator, which transport passengers
 */
public class Elevator {
    private final Semaphore semaphore;
    private final ReentrantLock lock = new ReentrantLock(true);

    private List<Passenger> elevatorContainer;
    private int storiesNumber;
    private int story;
    private int elevatorCapacity;

    /**
     * Creates a new Elevator with the given initial value.
     * @param storiesNumber the initial value
     * @param elevatorCapacity the initial value
     */
    public Elevator(final int storiesNumber, final int elevatorCapacity) {
        this.elevatorContainer = new ArrayList<>();
        this.story = 0;
        this.elevatorCapacity = elevatorCapacity;
        this.semaphore = new Semaphore(elevatorCapacity);
        this.storiesNumber = storiesNumber;
    }

    /**
     * Add passenger to elevator container
     * @param passenger which will add to elevator container
     */
    public void addPassenger(final Passenger passenger) {
        lock.lock();
        try {
            passenger.setTransportationState(TransportationState.IN_PROGRESS);
            elevatorContainer.add(passenger);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Gets and remove passenger from elevator container like example passenger
     * @param passengerExample example passenger
     * @return passenger from elevator container like example passenger
     */
    public Passenger getAndRemovePassenger(final Passenger passengerExample) {
        Passenger passenger;
        lock.lock();
        try {
            int index = elevatorContainer.indexOf(passengerExample);
            passenger = elevatorContainer.get(index);
            passenger.setTransportationState(TransportationState.COMPLETED);
            elevatorContainer.remove(index);
        } finally {
            lock.unlock();
        }
        return passenger;
    }
    /**
     * Gets passenger from elevator container like example passenger
     * @param passengerExample example passenger
     * @return passenger from elevator container like example passenger
     */
    public Passenger getPassenger(final Passenger passengerExample) {
        Passenger passenger;
        lock.lock();
        try {
            int index = elevatorContainer.indexOf(passengerExample);
            passenger = elevatorContainer.get(index);
        } finally {
            lock.unlock();
        }
        return passenger;
    }
    /**
     * Moves the elevator to the floor up
     */
    public void up() {
        this.story++;
    }

    /**
     * Moves the elevator to the floor down
     */
    public void down() {
        this.story--;
    }

    /**
     * Gets the semaphore current value.
     * @return semaphore current value
     */
    public Semaphore getSemaphore() {
        return semaphore;
    }

    /**
     * Gets the elevatorContainer current value.
     * @return elevatorContainer current value
     */
    public List<Passenger> getElevatorContainer() {
        return elevatorContainer;
    }

    /**
     * Gets the story current value.
     * @return story current value
     */
    public int getStory() {
        return story;
    }

    /**
     * Gets the storiesNumber current value.
     * @return storiesNumber current value
     */
    public int getStoriesNumber() {
        return storiesNumber;
    }

    /**
     * Gets the elevatorCapacity current value.
     * @return elevatorCapacity current value
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    @Override
    public String toString() {
        return "\nElevator{" + "elevatorContainer=" + elevatorContainer
                + ", story=" + story + ", elevatorCapacity="
                + elevatorCapacity + ", storiesNumber=" + storiesNumber + '}';
    }
}
