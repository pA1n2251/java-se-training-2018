package by.epam.beans;

/**
 * Transportation state
 */
public enum TransportationState {
    /**
     * Transportation not started
     */
    NOT_STARTED,
    /**
     * Transportation in progress
     */
    IN_PROGRESS,
    /**
     * Transportation complete
     */
    COMPLETED
}
