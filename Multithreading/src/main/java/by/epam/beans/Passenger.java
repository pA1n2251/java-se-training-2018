package by.epam.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * A passenger who moves in an elevator
 */
public class Passenger {
    private final Logger logger = LoggerFactory.getLogger(Passenger.class);

    private int passengerId;
    private int dispatchStory;
    private int destinationStory;
    private TransportationState transportationState;

    /**
     * Creates a new int with the given initial value.
     * @param passengerId the initial value
     * @param dispatchStory the initial value
     * @param destinationStory the initial value
     */
    public Passenger(final int passengerId, final int dispatchStory, final int destinationStory) {
        this.passengerId = passengerId;
        this.dispatchStory = dispatchStory;
        this.destinationStory = destinationStory;
        this.transportationState = TransportationState.NOT_STARTED;
    }

    /**
     * Gets the semaphore current value.
     * @return semaphore current value
     */
    public int getPassengerId() {
        return passengerId;
    }

    /**
     * Sets passengerId to the given value.
     * @param passengerId the passenger id
     */
    public void setPassengerId(final int passengerId) {
        this.passengerId = passengerId;
    }

    /**
     * Gets the semaphore current value.
     * @return semaphore current value
     */
    public int getDestinationStory() {
        return destinationStory;
    }

    /**
     * Sets destinationStory to the given value.
     * @param destinationStory destination story
     */
    public void setDestinationStory(final int destinationStory) {
        this.destinationStory = destinationStory;
    }

    /**
     * Gets the semaphore current value.
     * @return semaphore current value
     */
    public int getDispatchStory() {
        return dispatchStory;
    }

    /**
     * Sets dispatchStory to the given value.
     * @param dispatchStory dispatch story
     */
    public void setDispatchStory(final int dispatchStory) {
        this.dispatchStory = dispatchStory;
    }

    /**
     * Gets the semaphore current value.
     * @return semaphore current value
     */
    public TransportationState getTransportationState() {
        return transportationState;
    }

    /**
     * Sets transportationState to the given value.
     * @param transportationState transportation state
     */
    public void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Passenger passenger = (Passenger) o;
        return passengerId == passenger.passengerId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(passengerId);
    }

    @Override
    public String toString() {
        return "\nPassenger{" + "passengerId=" + passengerId
                + ", dispatchStory=" + dispatchStory
                + ", destinationStory=" + destinationStory
                + ", transportationState=" + transportationState + '}';
    }
}
