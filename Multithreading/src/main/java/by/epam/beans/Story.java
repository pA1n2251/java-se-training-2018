package by.epam.beans;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Story, on which there are containers
 */
public class Story {
    private final AtomicInteger number;
    private ConcurrentLinkedQueue<Passenger> dispatchStoryContainer;
    private ConcurrentLinkedQueue<Passenger> arrivalStoryContainer;

    /**
     * Creates a new Story with the given initial value.
     * @param number the initial value
     */
    public Story(final int number) {
        this.number = new AtomicInteger();
        this.number.addAndGet(number);
        this.dispatchStoryContainer = new ConcurrentLinkedQueue<>();
        this.arrivalStoryContainer = new ConcurrentLinkedQueue<>();
    }

    /**
     * Gets number the current value.
     *
     * @return the current value
     */
    public AtomicInteger getNumber() {
        return number;
    }

    /**
     * Gets dispatchStoryContainer the current value.
     * @return the current value
     */
    public ConcurrentLinkedQueue<Passenger> getDispatchStoryContainer() {
        return dispatchStoryContainer;
    }

    /**
     * Gets arrivalStoryContainer the current value.
     * @return the current value
     */
    public ConcurrentLinkedQueue<Passenger> getArrivalStoryContainer() {
        return arrivalStoryContainer;
    }

    @Override
    public String toString() {
        return "\nStory{" + "\ndispatchStoryContainer=" + dispatchStoryContainer
                + ", \narrivalStoryContainer=" + arrivalStoryContainer + '}';
    }
}
