package by.epam.beans;

import by.epam.Controller;
import by.epam.Initializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Building class
 */
public class Building {
    private final Logger logger = LoggerFactory.getLogger(Building.class);
    private final Semaphore semaphoreBoarding = new Semaphore(1);
    private final ReentrantLock lock;
    private final Condition conditionDeboard;
    private final int maxWait;

    private CopyOnWriteArrayList<Story> stories;
    private Controller controller;
    private int elevatorCapacity;
    private int passengersNumber;

    /**
     * Constructor without param
     */
    public Building() {
        Initializer initializer = new Initializer("src/main/resources/config.properties");
        this.controller = initializer.getController();
        this.stories = initializer.getStories();
        this.elevatorCapacity = initializer.getElevatorCapacity();
        this.passengersNumber = initializer.getPassengersNumber();
        lock = controller.getLock();
        conditionDeboard = controller.getConditionDeboard();
        maxWait = 2 * stories.size();
    }


    /**
     * Start transportation in this building
     */
    public void startTransportation() {
        boolean endTransportation;
        ExecutorService executor = Executors.newFixedThreadPool(passengersNumber);
        Runnable transportationTask = this::transport;
        logger.info("Start transportation");
        List<Future<?>> futures = Stream.generate(() -> executor.submit(transportationTask))
                .limit(passengersNumber).collect(Collectors.toList());
        try {
            do {
                controller.moveElevator();
                endTransportation = stories.stream().allMatch(s -> s.getDispatchStoryContainer().isEmpty());
                endTransportation = endTransportation & controller.getElevator().getElevatorContainer().isEmpty();
            } while (!endTransportation);
            for (Future<?> future: futures) {
                future.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            throw  new RuntimeException(e.getMessage(), e);
        }
        executor.shutdown();
        logger.info("End transportation");
    }

    private void transport() {
        //Initialization
        Elevator elevator = controller.getElevator();
        Semaphore semaphore = elevator.getSemaphore();
        Passenger passenger;
        Story story;
        int lastPassenger = 0;
        try {
            //Semaphore, which limits the number of passengers according to the capacity of the elevator
            semaphore.acquire();
            //Limitation so that all threads start at the same time
            controller.getCountDownLatch().countDown();
            controller.getCountDownLatch().await();
            try {
                //Semaphore, which allows only one person to enter the elevator
                semaphoreBoarding.acquire();
                story = stories.get(elevator.getStory());
                //Check if the floor is empty
                while (story.getDispatchStoryContainer().isEmpty()) {
                    controller.permitMove();
                    story = stories.get(elevator.getStory());
                    //Free last passenger
                    if (lastPassenger > maxWait) {
                        break;
                    }
                    lastPassenger++;
                }
                //Adding a passenger to the elevator container
                passenger = story.getDispatchStoryContainer().poll();
                controller.addPassengerToElevator(passenger);
            } finally {
                semaphoreBoarding.release();
            }
            //Waiting for destination floor
            while (passenger.getDestinationStory() != story.getNumber().get()) {
                lock.lock();
                try {
                    conditionDeboard.await();
                } finally {
                    lock.unlock();
                }
                story = stories.get(elevator.getStory());
            }
            //Unloading the passenger from the elevator container into the arrival container
            story.getArrivalStoryContainer().add(controller.getAndRemovePassengerFromElevator(passenger));
        } catch (InterruptedException e) {
            throw  new RuntimeException(e.getMessage(), e);
        } finally {
            semaphore.release();
        }
    }

    /**
     * Classical getter
     * @return stories this building
     */
    public CopyOnWriteArrayList<Story> getStories() {
        return stories;
    }

    /**
     * Classical getter
     * @return controller this building
     */
    public Controller getController() {
        return controller;
    }

    /**
     * Classical getter
     * @return elevator capacity this building
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * Classical getter
     * @return passenger number this building
     */
    public int getPassengersNumber() {
        return passengersNumber;
    }

    /**
     * Set passenger number
     * @param passengersNumber will set
     */
    public void setPassengersNumber(final int passengersNumber) {
        this.passengersNumber = passengersNumber;
    }

    @Override
    public String toString() {
        return "Building{" + "stories=" + stories
                + ", \ncontroller=" + controller + '}';
    }
}
