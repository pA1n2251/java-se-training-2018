package by.epam;

import by.epam.beans.Building;
import by.epam.beans.Elevator;
import by.epam.beans.Story;
import by.epam.beans.TransportationState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Validate end transportation
 */
public class Validator {
    private final Logger logger = LoggerFactory.getLogger(Validator.class);
    private Building building;

    /**
     * Creates a new Validator
     */
    public Validator() {
    }

    /**
     * Creates a new Validator with the given initial value.
     * @param building the initial value
     */
    public Validator(final Building building) {
        this.building = building;
    }

    /**
     * Validate end transportation
     */
    public void validate() {
        if (validateAllDispatchContainerEmpty(building.getStories())
                && validateElevatorContainerEmpty(building.getController().getElevator())
                && validatePassengersNumberInDispatchStoriesEqualPassengerNumber(building.getStories())
                && validateDestinationContainerEqualArrivalStoryAllPassengers(building.getStories())
                && validateAllPassengerInDestinationContainerComplete(building.getStories())
                ) {
            logger.info("Valid transportation");
        } else {
            logger.info("Valid fails");
        }
    }

    /**
     * Validate that all dispatch container is empty
     * @param stories with containers
     * @return result of validate in boolean format
     */
    public boolean validateAllDispatchContainerEmpty(final CopyOnWriteArrayList<Story> stories) {
        return stories.stream()
                .allMatch(s -> s.getDispatchStoryContainer().isEmpty());
    }

    /**
     * Validate that elevator container is empty
     * @param elevator from building
     * @return result of validate in boolean format
     */
    public boolean validateElevatorContainerEmpty(final Elevator elevator) {
        return elevator.getElevatorContainer().isEmpty();
    }

    /**
     * Validate that destination container equal arrival story in all passengers
     * @param stories with containers
     * @return result of validate in boolean format
     */
    public boolean validateDestinationContainerEqualArrivalStoryAllPassengers(
            final CopyOnWriteArrayList<Story> stories) {
        return stories.stream()
                .allMatch(story -> story.getArrivalStoryContainer().stream()
                        .allMatch(passenger -> passenger.getDestinationStory() == story.getNumber().get())
                );
    }

    /**
     * Validate that transportation state in all passenger in destination container is complete
     * @param stories with containers
     * @return result of validate in boolean format
     */
    public boolean validateAllPassengerInDestinationContainerComplete(final CopyOnWriteArrayList<Story> stories) {
        return stories.stream()
                .map(Story::getArrivalStoryContainer)
                .flatMap(ConcurrentLinkedQueue::stream)
                .allMatch(passenger -> passenger.getTransportationState() == TransportationState.COMPLETED);
    }

    /**
     * Validate that passengers number in dispatch stories equal passenger number
     * @param stories with containers
     * @return result of validate in boolean format
     */
    public boolean validatePassengersNumberInDispatchStoriesEqualPassengerNumber(
            final CopyOnWriteArrayList<Story> stories) {
        long size = stories.stream()
                .mapToInt(s -> s.getArrivalStoryContainer().size())
                .sum();
        return size == building.getPassengersNumber();
    }

    /**
     * Get building
     * @return building
     */
    public Building getBuilding() {
        return building;
    }

    /**
     * Set building
     * @param building that set
     */
    public void setBuilding(final Building building) {
        this.building = building;
    }
}
