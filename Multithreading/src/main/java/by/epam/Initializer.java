package by.epam;

import by.epam.beans.Passenger;
import by.epam.beans.Story;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Initializer, which init all class
 */
public class Initializer {
    private static final String STORIES_NUMBER = "storiesNumber";
    private static final String ELEVATOR_CAPACITY = "elevatorCapacity";
    private static final String PASSENGERS_NUMBER = "passengersNumber";

    private Properties property;

    /**
     * Creates a new Initializer
     * @param path to file with date
     */
    public Initializer(final String path) {
        this.property = new Properties();
        //Read property from property file
        try (FileInputStream fis = new FileInputStream(path)) {
            property.load(fis);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Gets storiesNumber read from file
     * @return storiesNumber read from file
     */
    public int getStoriesNumber() {
        return Integer.parseInt(property.getProperty(STORIES_NUMBER));
    }

    /**
     * Gets elevatorCapacity read from file
     * @return elevatorCapacity read from file
     */
    public int getElevatorCapacity() {
        return Integer.parseInt(property.getProperty(ELEVATOR_CAPACITY));
    }

    /**
     * Gets passengersNumber read from file
     * @return passengersNumber read from file
     */
    public int getPassengersNumber() {
        return Integer.parseInt(property.getProperty(PASSENGERS_NUMBER));
    }

    /**
     * Gets new controller
     * @return new controller
     */
    public Controller getController() {
        return new Controller(getStoriesNumber(), getElevatorCapacity());
    }

    /**
     * Gets new stories
     * @return new CopyOnWriteArrayList with random set passengers
     */
    public CopyOnWriteArrayList<Story> getStories() {
        CopyOnWriteArrayList<Story> stories = new CopyOnWriteArrayList<>();
        //Init array of stories
        for (int i = 0; i < getStoriesNumber(); i++) {
            stories.add(new Story(i));
        }
        //Init dispatchStoryContainer in all stories
        for (int i = 0; i < getPassengersNumber(); i++) {
            int dispatchStory;
            int destinationStory;
            //Cycle to provide that destinationStory != dispatchStory
            do {
                Random random = new Random();
                dispatchStory = random.nextInt(getStoriesNumber());
                destinationStory = random.nextInt(getStoriesNumber());
            } while (destinationStory == dispatchStory);
            stories.get(dispatchStory).getDispatchStoryContainer()
                    .add(new Passenger(i, dispatchStory, destinationStory));
        }
        return stories;
    }
}
